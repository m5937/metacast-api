# Metacast API

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![CI Status](https://gitlab.com/m5937/metacast-api/badges/main/pipeline.svg)](https://gitlab.com/m5937/metacast-api/-/pipelines)




### Setup
1. Ensure you have the following locally installed:
  - python (3.9)
  - pip
  - pipenv
  - docker
  - docker-compose
2. Install dev dependancies locally: `pipenv install`
3. Run services with `docker-compose up`

You should be able to access the API at http://localhost:8000

Useful stuff:
- Load dev data from requirements with `python manage.py load_dev_data`
- Check your work with `./scripts/qa`
- Run tests with `python manage.py test --parallel 16` 
- Set your precommit hook with `cp ./scripts/pre-commit .git/hooks/pre-commit`


### API Documentation
Swagger page available at: 
  http://localhost:8000/docs/

This will also allow you to make requests to the API.

### Library Documentation
Project:
- [Docker Compose](https://docs.docker.com/compose/gettingstarted/)
- [Pipenv](https://pipenv.pypa.io/en/latest/basics/)
- [Arrow](https://arrow.readthedocs.io/en/latest/)
 
Django:
- [Django](https://docs.djangoproject.com/en/3.2/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [DRF Spectacular](https://github.com/tfranzel/drf-spectacular)
- [Django Filters](https://django-filter.readthedocs.io/en/stable/guide/usage.html)
- [Django Nested Routers](https://github.com/alanjds/drf-nested-routers)

### Docker Image
The Gitlab CI pipeline automatically builds the docker container
and pushes it to this repository:
    https://hub.docker.com/r/fargusplumdoodle/metacast_api

You can pull it with: `docker pull fargusplumdoodle/metacast_api`



# Project Writeup
This sections outlines the process of making this app.


## Design Considerations

**Django / DRF**

I chose Django/DRF due to its ease of use and out of the box functionality. I have been working
with Django for 2 years and am very familiar with its inner workings.

**Postgresql**

The requirements on what to do with the database were very loose. I went with Postgresql because it is a
very powerful open source relational database that integrates with Django quite well.

In terms of table layout, I kept the game listing as a single table. I denormalized the `images` and `tags` 
fields to reduce joins. We only store the URLs of the images, the API should not serve images/videos/volumetric data
those would be stored on another service. 

I think its possible the `author` field could be a foreign key to its own table. But since we don't have more 
information on authors, I think its fine as its own column.

**Gitlab CI**

Gitlab CI is very simple and allows me to run my builds on my local server. I have used Circleci but I
like that this is integrated with the rest of my Gitlab server.

**Documentation**

This was my first time using the `drf-spectacular` library. I am incredibly impressed with its
ability to read docstrings as markdown and integrate with Swagger.

**Premium Support**

I am not implementing support for premium users/premium games on
the Unity front end because I am not very familiar with Unity/C# and
do not want to submit a substandard product. However, I did implement
functionality for it on the API as that is my specialty.

For determining if a user is premium, I just have a boolean on the user
table. However, if this were a real project, I would likely have a
subscription system that integrates with billing software like Zoho.

I have also made the game listing endpoints fully open so anyone on
the internet can perform read only operations on games, however only 
premium users who are authenticated can access premium games.

In addition to this I have added CRUD functionality for staff users.

**Feed endpoint**

This endpoint would be called every time a user opens their application, showing
games that are in the near future and that are currently running. 

Its possible you would also want to show some previous games, but that depends on
the project requirements.

The requirements did not specify including the time that a game is running, but I felt that
it was necessary information about the games. We have the duration already, so I used that
to calculate when the game ends. 


## Performance / Scalability

I did not go overboard on the performance tuning side of things. It is said that
premature optimization is the root of all evil. 
I would prefer to:
1. Make a good, well tested project
2. Slowly roll out to users
3. Monitor for pain-points
4. Optimize
5. Repeat until we are satisfied with the user experience

That said there are a few things that I have done to ensure performance and scalabiltiy:
- Throttle rates (need to be tuned to the clients)
- Pagination on list endpoints
- Index on the `startTime` and `endTime` columns, because the feed endpoint will be
  called quite frequently and this will reduce load on the database.


## Challenges

The API side of things I am very comfortable with, so I didn't encounter any 
challenges as I have done many Django projects before.

The Unity side was a bit of a struggle. The biggest issue was trying to 
make Unity wait for the requests to complete before initializing the
carousel. I haven't touched Unity since my gaming class in college. I'm sure
there is a lot that I could do better. 

I did also encounter a minor issue with Gitlab CI, on the public gitlab 
servers there is a bug with docker in docker. So I made it so only my local
gitlab will build the docker containers.


## Client side improvements

If I were more proficient in Unity these are the improvements
I would have made to the UI Browser:
- Loading screen while waiting for network
- Simultaneously fetched the images all at once. Getting them 
  sequentially is slow
- Implemented authentication. I would have added OAuth with 
  the PKCE flow.
- I would also make it so as you scrolled through the game listings
  the application would request more pages from the API. This would
  significantly reduce the load on the server, making the system as
  a whole more scalable.
  

## API Improvements
If I had more time this is what I would implement:
- Helm chart for deploying to Kubernetes
- Add a deployment script to the CI pipeline
- OAuth
- Logging (There wasn't any logic that required logging, so it got left out)