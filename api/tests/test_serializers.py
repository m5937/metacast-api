import json
from copy import deepcopy

from django.conf import settings

from api.models import GameListing
from api.serializers import GameListingSerializer
from project.utils.test import MetacastTestCase


class TestGameListingSerializer(MetacastTestCase):
    @classmethod
    def setUpTestData(cls):
        with open(settings.EXAMPLE_GAME_LISTINGS_PATH, "r") as f:
            cls.example_game_listings = json.loads(f.read())

    def test_write(self):
        data = self.example_game_listings[0]
        s = GameListingSerializer(data=data)
        self.assertTrue(s.is_valid())
        s.save()
        self.assertTrue(GameListing.objects.filter(title=data["title"]).exists())

    def test_invalid_image_type(self):
        data = deepcopy(self.example_game_listings[0])
        data["images"][0]["type"] = 99999  # non valid type
        s = GameListingSerializer(data=data)
        self.assertFalse(s.is_valid())
        self.assertIn("type", s.errors["images"][0])

    def test_invalid_image_missing_fields(self):
        required_fields = ["type", "id", "url"]

        for field in required_fields:
            data = deepcopy(self.example_game_listings[0])
            del data["images"][0][field]

            s = GameListingSerializer(data=data)
            self.assertFalse(s.is_valid())
            self.assertIn(field, s.errors["images"][0])

    def test_no_images(self):
        data = deepcopy(self.example_game_listings[0])
        data["images"] = []
        s = GameListingSerializer(data=data)
        self.assertTrue(s.is_valid())

    def test_read(self):
        game = self.generate_game_listing()
        s = GameListingSerializer(game)
        self.assert_dict_equals_game(s.data, game)
