import arrow
from project.utils.test import MetacastTestCase


class TestGameListingSignals(MetacastTestCase):
    def test_set_end_time(self):
        game = self.generate_game_listing(
            startTime=self.now.datetime,
            duration=2 * 24 * 60,  # runtime of two days
        )
        self.assertEqual(arrow.get(game.endTime), self.now.shift(days=2))
