import json
from copy import deepcopy

import arrow
from django.conf import settings
from rest_framework.reverse import reverse

from api.models import GameListing
from project.utils.test import MetacastTestCase


class TestGameListingViewSet(MetacastTestCase):
    list_url = "api:games-list"
    detail_url = "api:games-detail"

    @classmethod
    def setUpTestData(cls):
        cls.staff_user = cls.generate_user(username="staff", is_staff=True)
        cls.non_premium_user = cls.generate_user(username="non_premium")
        cls.premium_user = cls.generate_user(username="premium", is_premium=True)

        cls.non_staff_users = [cls.non_premium_user, cls.premium_user, None]

        with open(settings.EXAMPLE_GAME_LISTINGS_PATH, "r") as f:
            cls.example_game_listings = json.loads(f.read())


class TestGameListingViewSetPermissions(TestGameListingViewSet):
    """
    Ensures that non staff users have read only access to the API
    """

    def setUp(self):
        self.non_premium_game = self.generate_game_listing()
        self.premium_game = self.generate_game_listing(premium=True)

    def test_non_staff_permissions(self):
        game_data = self.example_game_listings[0]
        url = reverse(self.detail_url, args=[self.premium_game.pk])

        for user in self.non_staff_users:
            # Non authenticated users will receive a 401 error
            expected_status = 403 if user else 401

            r = self.post(reverse(self.list_url), data=game_data, user=user)
            self.assertEqual(r.status_code, expected_status, user)

            r = self.delete(url, user=user)
            self.assertEqual(r.status_code, expected_status, user)

            r = self.put(url, data=game_data, user=user)
            self.assertEqual(r.status_code, expected_status, user)

            r = self.patch(url, data=game_data, user=user)
            self.assertEqual(r.status_code, expected_status, user)

    def test_create(self):
        for game_data in self.example_game_listings:
            r = self.post(reverse(self.list_url), data=game_data, user=self.staff_user)
            self.assertEqual(r.status_code, 201)

            game = GameListing.objects.get(pk=r.json()["id"])
            self.assert_dict_equals_game(game_data, game)

    def test_update(self):
        game_data = deepcopy(self.example_game_listings[0])
        game_data["title"] = "some thing else"
        url = reverse(self.detail_url, args=[self.premium_game.pk])
        r = self.put(url, data=game_data, user=self.staff_user)

        self.assertEqual(r.status_code, 200)
        self.premium_game.refresh_from_db()
        self.assert_dict_equals_game(game_data, self.premium_game)

    def test_partial_update(self):
        game_data = {"title": "different"}
        url = reverse(self.detail_url, args=[self.premium_game.pk])
        r = self.patch(url, data=game_data, user=self.staff_user)

        self.assertEqual(r.status_code, 200)
        self.premium_game.refresh_from_db()
        self.assertEqual(self.premium_game.title, game_data["title"])

    def test_delete(self):
        url = reverse(self.detail_url, args=[self.premium_game.pk])
        r = self.delete(url, user=self.staff_user)
        self.assertEqual(r.status_code, 204)
        self.assertFalse(GameListing.objects.filter(pk=self.premium_game.pk).exists())

    def test_list_non_premium_user(self):
        for user in [None, self.non_premium_user]:
            r = self.get(reverse(self.list_url), user=user)
            self.assertEqual(r.status_code, 200)
            data = r.json()
            self.assertEqual(data["results"][0]["id"], self.non_premium_game.id)
            self.assertEqual(data["count"], 1)

    def test_list_premium_user(self):
        for user in [self.staff_user, self.premium_user]:
            r = self.get(reverse(self.list_url), user=user)
            self.assertEqual(r.status_code, 200)
            data = r.json()
            games_returned = {game["id"] for game in data["results"]}
            expected_games = {self.non_premium_game.id, self.premium_game.id}
            self.assertEqual(games_returned, expected_games)

    def test_list_ordering(self):
        GameListing.objects.all().delete()

        expected_game_pks_in_order = [
            self.generate_game_listing(startTime=self.now.shift(days=2).datetime).pk,
            self.generate_game_listing(startTime=self.now.shift(days=1).datetime).pk,
            self.generate_game_listing(startTime=self.now.datetime).pk,
        ]

        r = self.get(reverse(self.list_url), user=self.non_premium_user)
        self.assertEqual(r.status_code, 200)
        data = r.json()
        found_ids = [game["id"] for game in data["results"]]
        self.assertEqual(found_ids, expected_game_pks_in_order)


class TestViewGameSetUserLogic(TestGameListingViewSet):
    feed_url = "api:games-feed"

    def setUp(self):
        self.past_game = self.generate_game_listing(
            startTime=self.now.shift(days=-1).datetime
        )
        self.running_game = self.generate_game_listing(
            startTime=self.now.shift(days=-1).datetime,
            duration=2 * 24 * 60,  # runtime of two days
        )
        self.future_game = self.generate_game_listing(
            startTime=self.now.shift(days=1).datetime
        )

    def test_feed(self):
        r = self.get(reverse(self.feed_url))
        self.assertEqual(r.status_code, 200)
        data = r.json()
        games_returned = {game["id"] for game in data["results"]}
        expected_games = {self.running_game.id, self.future_game.id}
        self.assertEqual(games_returned, expected_games)


class TestViewGameFilterset(TestGameListingViewSet):
    def setUp(self):
        self.tags = ["mma", "ufc", "Kevin Lee"]
        self.game = self.generate_game_listing(tags=self.tags)

    def assert_fetches_games(self, expected_games, query_params):
        r = self.get(reverse(self.list_url), query=query_params, user=self.premium_user)
        self.assertEqual(r.status_code, 200, r.json())
        data = r.json()
        self.assertEqual(data["count"], len(expected_games))
        game_ids_returned = {game["id"] for game in data["results"]}
        expected_game_ids = {game.id for game in expected_games}
        self.assertEqual(game_ids_returned, expected_game_ids)

    def test_simple_filters(self):
        attributes = [
            "id",
            "title",
            "subtitle",
            "category",
            "type",
            "author",
            "premium",
            "isDownloadable",
            "isStreamable",
        ]
        for attribute in attributes:
            self.assert_fetches_games(
                [self.game], {attribute: getattr(self.game, attribute)}
            )

    def test_time_filters(self):
        start_time = arrow.get(self.game.startTime)
        end_time = arrow.get(self.game.endTime)
        queries = [
            {"startTime": start_time.isoformat()},
            {"startTime__gte": start_time.shift(seconds=-1).isoformat()},
            {"startTime__lte": start_time.shift(seconds=1).isoformat()},
            {"endTime": end_time.isoformat()},
            {"endTime__gte": end_time.shift(seconds=-1).isoformat()},
            {"endTime__lte": end_time.shift(seconds=1).isoformat()},
        ]
        for query in queries:
            self.assert_fetches_games([self.game], query)

    def test_tag_filters(self):
        another_game = self.generate_game_listing(tags=self.game.tags)

        # wont match
        self.generate_game_listing(tags=["bouldering", "climbing"])

        for tag in self.tags:
            self.assert_fetches_games([another_game, self.game], {"tags": tag})

        self.assert_fetches_games(
            [another_game, self.game], [("tags", tag) for tag in self.tags]
        )
