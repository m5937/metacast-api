from typing import List

from django.db.models import QuerySet
from django_filters import rest_framework as filterset

from api.enums import Categories, GameTypes
from project.utils.filters import CharListFilter


class GameListingFilterSet(filterset.FilterSet):
    id = filterset.NumberFilter()
    title = filterset.CharFilter()
    subtitle = filterset.CharFilter()
    category = filterset.ChoiceFilter(choices=Categories.choices())
    type = filterset.ChoiceFilter(choices=GameTypes.choices())
    author = filterset.CharFilter()
    premium = filterset.BooleanFilter()
    isDownloadable = filterset.BooleanFilter()
    isStreamable = filterset.BooleanFilter()

    startTime = filterset.DateTimeFilter()
    startDateTime__gte = filterset.DateTimeFilter(lookup_expr="gte")
    startDateTime__lte = filterset.DateTimeFilter(lookup_expr="lte")
    endDateTime = filterset.DateTimeFilter()
    endDateTime__gte = filterset.DateTimeFilter(lookup_expr="gte")
    endDateTime__lte = filterset.DateTimeFilter(lookup_expr="lte")

    tags = CharListFilter(lookup_expr="contains", method="tags_filter")

    @staticmethod
    def tags_filter(queryset: QuerySet, _, value: List[str]) -> QuerySet:
        return queryset.filter(tags__contains=value)
