import json
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from api.models import MetacastUser
from api.serializers import GameListingSerializer


class Command(BaseCommand):
    help = "Pre-populate the test database with some commonly used objects"

    @staticmethod
    def _load_game_listings():
        with open(settings.EXAMPLE_GAME_LISTINGS_PATH, "r") as f:
            data = json.loads(f.read())

        serializer = GameListingSerializer(data=data, many=True)
        assert serializer.is_valid()
        serializer.save()

    @staticmethod
    def _load_users():
        users = [
            {"username": "staff", "is_staff": True},
            {"username": "premium", "is_premium": True},
            {"username": "non_premium"},
        ]
        for user_data in users:
            user, _ = MetacastUser.objects.get_or_create(**user_data)
            user.set_password(user.username)

    def handle(self, *args, **options):
        if not settings.DEBUG:
            self.stderr.write(
                self.style.ERROR("Refusing to create dev data in production")
            )
            raise CommandError(-1)

        self._load_game_listings()
        self._load_users()
        self.stdout.write(self.style.SUCCESS("DONE"))
