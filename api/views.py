import arrow

from django.db.models import QuerySet, Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response

from api.filters import GameListingFilterSet
from api.models import GameListing, MetacastUser
from api.serializers import GameListingSerializer

from rest_framework.permissions import IsAdminUser, AllowAny


class GameListingViewSet(viewsets.ModelViewSet):
    """
    # GameListings
    *For performing CRUD operations on game listings.*

    Only premium users can access premium games. Only staff can create/update/delete games.
    """

    serializer_class = GameListingSerializer
    filterset_class = GameListingFilterSet

    def get_permissions(self) -> list:
        if self.action not in ("list", "retrieve", "feed"):
            return [IsAdminUser()]
        return [AllowAny()]

    def get_queryset(self) -> QuerySet:
        user = self.request.user
        games = GameListing.objects.all()

        if user.is_staff or (isinstance(user, MetacastUser) and user.is_premium):
            return games

        return games.filter(premium=False)

    @action(detail=False, methods=["GET"], url_path="feed")
    def feed(self, request: Request, *args, **kwargs):
        """
        # Feed endpoint

        Only returns games that are in the future, or are currently running
        """
        now = arrow.now("UTC").datetime

        games = self.get_queryset().filter(
            Q(startTime__gte=now) | Q(startTime__lte=now, endTime__gte=now)
        )

        page = self.paginate_queryset(games)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(games, many=True)
        return Response(serializer.data)
