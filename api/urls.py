from rest_framework_nested import routers
from api.views import GameListingViewSet

app_name = "api"

router = routers.DefaultRouter()
router.register("games", GameListingViewSet, basename="games")

urlpatterns = router.urls
