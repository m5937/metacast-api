import arrow
from django.db.models.signals import pre_save
from django.dispatch import receiver

from api.models import GameListing


@receiver(pre_save, sender=GameListing)
def set_end_time(instance: GameListing, **_):
    instance.endTime = (
        arrow.get(instance.startTime).shift(minutes=float(instance.duration)).datetime
    )
