from project.utils.models import ChoiceEnum


class Categories(ChoiceEnum):
    LIVE = "live"
    UPCOMING = "upcoming"
    RECOMMENDED = "recommended for you"
    CONTINUE_WATCHING = "continue watching"


class ImageTypes(ChoiceEnum):
    BACKGROUND = 1
    FEATURED = 2
    LOGO = 3
    ICON = 4


class GameTypes(ChoiceEnum):
    UFC = 1
    BOULDERING = 2
    SOCCER = 3


class Versions(ChoiceEnum):
    V1_0 = "1.0"
