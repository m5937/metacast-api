from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.db import models

from api.enums import Categories, GameTypes, Versions


class GameListing(models.Model):
    """
    Defines a game for a given sport
    """

    title = models.CharField(max_length=50)
    subtitle = models.CharField(max_length=50)
    category = Categories.char_field(default=Categories.UPCOMING.value)
    type = models.IntegerField(
        choices=GameTypes.choices(), default=GameTypes.BOULDERING.value
    )

    startTime = models.DateTimeField()
    endTime = models.DateTimeField(null=True)

    description = models.TextField()
    images = models.JSONField(default=list)

    # Metadata
    tags = ArrayField(models.CharField(max_length=20), default=list)
    author = models.CharField(max_length=30)
    replayBundleUrlJson = models.URLField(blank=True)
    duration = models.DecimalField(max_digits=20, decimal_places=15)  # minutes
    premium = models.BooleanField(default=False)
    isDownloadable = models.BooleanField(default=False)
    isStreamable = models.BooleanField(default=True)
    version = Versions.char_field(default=Versions.V1_0)

    class Meta:
        ordering = ["-startTime"]
        indexes = [models.Index(fields=["startTime", "endTime"])]


class MetacastUser(AbstractUser):
    """
    User class for this API
    """

    is_premium = models.BooleanField(default=False)
