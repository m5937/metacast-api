from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.enums import ImageTypes
from api.models import GameListing


class ImageSerializer(serializers.Serializer):
    id = serializers.CharField()
    url = serializers.URLField()
    type = serializers.IntegerField()

    @staticmethod
    def validate_type(value):
        choices = [image_type[0] for image_type in ImageTypes.choices()]
        if value not in choices:
            raise ValidationError(f"Invalid choice, must be in: {choices}")
        return value


class GameListingSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = GameListing
        fields = "__all__"
