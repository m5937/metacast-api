#!/bin/bash
set -ex

python3.9 -m venv ./venv
./venv/bin/pip install -U pip pipenv
./venv/bin/pipenv lock \
	--dev \
	--keep-outdated \
	--requirements > /requirements.txt 
./venv/bin/pip install -U pip install -r /requirements.txt
