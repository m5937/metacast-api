from typing import Type, Optional
from urllib.parse import urlencode

import arrow
from django.db.models import Model
from rest_framework.authtoken.models import Token

from api.enums import ImageTypes, Versions
from api.models import MetacastUser, GameListing
from rest_framework.test import APITestCase


class GeneratorMixin:
    """
    Contains a set of functions for generating test data
    """

    now: arrow.Arrow

    @staticmethod
    def unique_val(model: Type[Model], label: Optional[str] = None):
        if not label:
            label = model.__name__
        return f"{label}_{model.objects.count()}"

    @classmethod
    def generate_user(cls, **kwargs) -> MetacastUser:
        defaults = {"username": cls.unique_val(MetacastUser), **kwargs}
        return MetacastUser.objects.create(**defaults)

    @classmethod
    def generate_game_listing(cls, **kwargs) -> GameListing:
        GameListing.startTime.editable = True
        defaults = {
            "title": cls.unique_val(GameListing, "title"),
            "subtitle": cls.unique_val(GameListing, "subtitle"),
            "startTime": cls.now.shift(days=1).datetime,
            "description": "Lorem ipsum dolor sit amet...",
            "images": [
                {
                    "id": "1",
                    "url": "https://example.com/img.png",
                    "type": ImageTypes.BACKGROUND.value,
                }
            ],
            "tags": ["mma", "fight", "GSP", "Kevin Lee"],
            "author": "Stephen King",
            "replayBundleUrlJson": "https://example.com/img.png",
            "duration": 16.049999237060548,
            "premium": False,
            "isDownloadable": False,
            "isStreamable": True,
            "version": Versions.V1_0.value,
            **kwargs,
        }
        return GameListing.objects.create(**defaults)


class NetworkMixin:
    """
    Contains a set of functions for making authenticated requests in testing
    """

    def _make_request(
        self, method_name, endpoint, data, encoding="json", query=None, user=None
    ):
        """Make a generic http request using the test client"""
        http_method = getattr(self.client, method_name)
        kwargs = {"data": data, "format": encoding}

        if user:
            token, _ = Token.objects.get_or_create(user=user)
            kwargs["HTTP_AUTHORIZATION"] = f"Token {token}"

        if query is not None:
            endpoint += "?" + urlencode(query)

        return http_method(endpoint, **kwargs)

    def get(self, endpoint, query=None, user=None):
        return self._make_request("get", endpoint, None, "json", query=query, user=user)

    def delete(self, endpoint, query=None, user=None):
        return self._make_request(
            "delete", endpoint, None, "json", query=query, user=user
        )

    def post(self, endpoint, data, encoding="json", query=None, user=None):
        return self._make_request(
            "post", endpoint, data, encoding, query=query, user=user
        )

    def put(self, endpoint, data, encoding="json", query=None, user=None):
        return self._make_request(
            "put", endpoint, data, encoding, query=query, user=user
        )

    def patch(self, endpoint, data, encoding="json", query=None, user=None):
        return self._make_request(
            "patch", endpoint, data, encoding, query=query, user=user
        )


class MetacastTestCase(APITestCase, GeneratorMixin, NetworkMixin):
    """
    Base test case for the metacast API. Contains a lot of helper functions to make testing easier.
    """

    now = arrow.now("UTC")

    def assert_dict_equals_game(self, game_data: dict, game: GameListing) -> None:
        """
        Ensures a dictionary of a game listing is equal to the GameListing object
        """
        duration = game_data["duration"]
        self.assertAlmostEqual(float(game.duration), float(duration), 5)

        start_time = arrow.get(game_data["startTime"]).datetime
        self.assertEqual(game.startTime, start_time)

        end_time = game_data.get("endTime")
        if end_time:
            end_time = arrow.get(end_time).datetime
            self.assertEqual(game.endTime, end_time)

        # other fields should be exactly the same
        for key, value in game_data.items():
            if key in ["duration", "startTime", "endTime"]:
                continue

            self.assertEqual(getattr(game, key), value, key)
