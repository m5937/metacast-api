from django import forms
from django_filters import MultipleChoiceFilter


class CharListFilter(MultipleChoiceFilter):
    """
    Accepts multiple values for the same key in the format ?n=v1&n=v2

    Behaves exactly like the MultipleChoiceFilter, except the `choices`
    parameter is no longer required. Forms the OR of the given values, unless
    the `conjoined=True` argument is given in which case AND will be used.
    """

    class CharListField(forms.MultipleChoiceField):
        """Remove required choices from native MultipleChoiceField"""

        def valid_value(self, value) -> bool:
            return isinstance(value, str)

    field_class = CharListField
